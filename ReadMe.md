Project Goals
-------------

By default, the Linux distribution 'Slackware Linux' does not include any repository for third-party software packages.
Each administrator is supposed to compile her/his own programs from source. To do this in a more systematic manner, so-called Slackbuild scripts, i.e., compilation scripts are available on the website [https://www.slackbuilds.org](https://www.slackbuilds.org). These scripts download required sources, build the executables and create installable slackware packages.
But for each script, the administrator has to check dependencies manually by recursively looking up the requirements-information on the website.

The goal of this project is a python script that automatizes this process and returns a list of dependencies and a command string for the administrator to perform the whole process of downloading, building and installing all (not yet installed) packages in the required order.



Commands
--------

To check the dependencies of a package named 'pkgname', use the following command:

python3 fetch_deps.py pkgname

Example:

<img src="Screenshot.png">



How the Algorithm Works
-----------------------

The script 'crawls' through the website [https://www.slackbuilds.org](https://www.slackbuilds.org) in a recursive manner.
Starting with the sought package, it looks up the according information page online and parses the 'This requires' field.
While doing this, a so-called level is assigned to the package in the following recursive way:  
If there is no dependency, the level is set to zero.  
Else, the level is set to the maximum level of all its dependency-packages plus one.

Finally, the python list of dependency-packages is sorted by the assigned levels in ascending order.

To download the HTML pages, urllib.request is used.
