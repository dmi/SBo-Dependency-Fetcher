#!/usr/bin/python3

# Set the slackware version here:
sw_ver = "15.0"


from   urllib.request import urlopen
import sys


color_reset = "\x1b[0m"
color_cyan  = "\x1b[0;36m"
color_red   = "\x1b[0;31m"


class Cnode:
  
  def __init__(self, name, link, level):
    
    self.name  = name
    self.link  = link
    self.level = level


def find_substring(text, start_sequence, end_sequence):
  return text.split(start_sequence.encode("utf-8"))[1].split(end_sequence.encode("utf-8"))[0]


def get_first_link(first_name):
  
  search_link   = "https://www.slackbuilds.org/result/?search=" + first_name + "&sv=" + sw_ver
  search_result = urlopen(search_link).read()
  
  if not b"Search Results" in search_result:
    
    first_link = search_link
    
  else:
    
    text_area = find_substring(search_result, "Search Results", "include-bottom")
    
    entries = text_area.split("onMouseOver".encode("utf-8"))
    
    for entry in entries[1:]:
      
      link = "https://slackbuilds.org" + entry.split("href=\"".encode("utf-8"))[1].split("\"".encode("utf-8"))[0].decode()
      
      if first_name + "/" in link:
        first_link = link
        break
  
  return first_link


def req_deps(text):
  
  dep_list = []
  
  if b"This requires:" in text:
    
    entries = find_substring(text, "This requires:", "</p>").split(",".encode("utf-8"))
    
    for entry in entries:
      
      name = entry.split(">".encode("utf-8"))[1].split("<".encode("utf-8"))[0].decode()
      link = "https://slackbuilds.org" + entry.split("\'".encode("utf-8"))[1].decode()
      
      dep_list.append([name, link])
  
  return dep_list


def expand_recursively(name, link):
  
  global no_of_pkgs
  global checked_pkgs
  
  print(color_cyan + "Looking up '" + name + "'." + color_reset)
  
  level = 0
  
  pkg_info_source = urlopen(link).read()
  req_dep_list    = req_deps(pkg_info_source)
  
  for dep in req_dep_list:
    
    dep_name = dep[0]
    dep_link = dep[1]
    
    if not dep_name in checked_pkgs:
      expand_recursively(dep_name, dep_link)
    
    dep_level = checked_pkgs[dep_name].level
    if dep_level >= level - 1:
      level = dep_level + 1
  
  pkg = Cnode(name, link, level)
  checked_pkgs[name] = pkg
  no_of_pkgs += 1
  
  print(color_cyan + "... Level of '" + name + "' is " + str(level) + "." + color_reset)


print()

checked_pkgs = {}

first_name = sys.argv[1]
first_link = get_first_link(first_name)

no_of_pkgs = 0

expand_recursively(first_name, first_link)

pkgs_to_install = list(checked_pkgs.values())
pkgs_to_install.sort(key = lambda x : x.level)

print("\n" + color_cyan + str(no_of_pkgs) + " packages must be installed (minus those that are already there)." + color_reset)
print("\n" + color_cyan + "Use the following order:\n" + color_reset)

pkg_sequence = ""
for pkg in pkgs_to_install:
  pkg_sequence += pkg.name + " "
pkg_sequence = pkg_sequence[:-1]
print(pkg_sequence)

print("\n" + color_cyan + "Assuming " + color_reset + "sbopkg -r"
      + color_cyan + " has already been run, one could use" + color_reset + "\n")

print("sbopkg -k -i \"" + pkg_sequence + "\"")

print("\n" + color_cyan + "to install all of them. (The \"-k\" option prevents reinstallation.)" + color_reset)
print("\n" + color_red + "Be careful though and read the documentation of all packages and scripts before installing!" + color_reset + "\n")

print(color_cyan + "The following commands might be useful to check previous installations:" + color_reset + "\n")
print("cd /var/lib/pkgtools/packages")
for pkg in pkgs_to_install:
  print("ls | grep -i " + pkg.name)
print()
